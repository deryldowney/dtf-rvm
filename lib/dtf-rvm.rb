require "dtf-rvm/version"

module Dtf
load "#{File.join(File.dirname(__FILE__), "/config/environment.rb")}"

  module Rvm
    require "rvm"
  end
end
